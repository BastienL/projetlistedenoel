<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210120111232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C19357C6E5');
        $this->addSql('DROP INDEX IDX_64C19C19357C6E5 ON category');
        $this->addSql('ALTER TABLE category DROP gifts_id');
        $this->addSql('ALTER TABLE gift ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE gift ADD CONSTRAINT FK_A47C990D12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_A47C990D12469DE2 ON gift (category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category ADD gifts_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C19357C6E5 FOREIGN KEY (gifts_id) REFERENCES gift (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_64C19C19357C6E5 ON category (gifts_id)');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990D12469DE2');
        $this->addSql('DROP INDEX IDX_A47C990D12469DE2 ON gift');
        $this->addSql('ALTER TABLE gift DROP category_id');
        $this->addSql('ALTER TABLE personne CHANGE sex sex VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
