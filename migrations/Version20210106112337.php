<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210106112337 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, gifts_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_64C19C19357C6E5 (gifts_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift (id INT AUTO_INCREMENT NOT NULL, designation LONGTEXT DEFAULT NULL, name VARCHAR(255) NOT NULL, minimum_age INT DEFAULT NULL, average_price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_personne (gift_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_49CB2DA597A95A83 (gift_id), INDEX IDX_49CB2DA5A21BD112 (personne_id), PRIMARY KEY(gift_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C19357C6E5 FOREIGN KEY (gifts_id) REFERENCES gift (id)');
        $this->addSql('ALTER TABLE gift_personne ADD CONSTRAINT FK_49CB2DA597A95A83 FOREIGN KEY (gift_id) REFERENCES gift (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_personne ADD CONSTRAINT FK_49CB2DA5A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C19357C6E5');
        $this->addSql('ALTER TABLE gift_personne DROP FOREIGN KEY FK_49CB2DA597A95A83');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE gift');
        $this->addSql('DROP TABLE gift_personne');
        $this->addSql('ALTER TABLE personne CHANGE sex sex VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
