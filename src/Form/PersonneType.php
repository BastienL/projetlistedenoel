<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_surname', TextType::class, [
                'attr' => [
                    'disabled' => $options['e']
                ]
            ])
            ->add('sex', ChoiceType::class, array(
                'choices' => array('Male' => 'male', 'Female' => 'female', 'i don\'t want indicate it' => 'not indicated'),
            ))
            ->add('date_of_birth', BirthdayType::class)
            ->add('address', AddressType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
            'e' => false
        ]);
    }
}
