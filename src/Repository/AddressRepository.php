<?php

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    public function findIfExist(Address $address)
    {
        $city = trim($address->getCity());
        $street = trim($address->getStreet());
        $zipCode = trim($address->getZipCode());
        $number = trim($address->getNumber());

        $tag = $this->createQueryBuilder('a')
            ->andWhere('a.city = :city')
            ->setParameter('city', $city)
            ->andWhere('a.street = :street')
            ->setParameter('street', $street)
            ->andWhere('a.zip_code = :zipCode')
            ->setParameter('zipCode', $zipCode)
            ->andWhere('a.number = :number')
            ->setParameter('number', $number)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $tag;
    }

    public function findByCity(string $city)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.city = :city')
            ->setParameter('city', $city)
            ->getQuery()
            ->getResult();
    }

    public function findByStreetAndCity(string $street, int $zipCode)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.street = :street')
            ->setParameter('street', $street)
            ->andWhere('a.zip_code = :zip_code')
            ->setParameter('zip_code', $zipCode)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Address[] Returns an array of Address objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


//    public function findOneBySomeField($value): ?Address
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.city = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

}
