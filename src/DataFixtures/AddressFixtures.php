<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AddressFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $address = new Address();
        $address->setZipCode(80000);
        $address->setNumber(10);
        $address->setStreet('Boulevard de Dury');
        $address->setCity('Amiens');
        $manager->persist($address);

        $address = new Address();
        $address->setZipCode(76000);
        $address->setNumber(22);
        $address->setStreet("Route d'Amiens");
        $address->setCity('Rouen');
        $manager->persist($address);

        $address = new Address();
        $address->setZipCode(80000);
        $address->setNumber(10);
        $address->setStreet('Route de Paris');
        $address->setCity('Amiens');
        $manager->persist($address);

        $address = new Address();
        $address->setZipCode(51100);
        $address->setNumber(10);
        $address->setStreet('Rue Bruyant');
        $address->setCity('Reins');
        $manager->persist($address);

        $address = new Address();
        $address->setZipCode(76100);
        $address->setNumber(10);
        $address->setStreet("Boulevard de l'Europe");
        $address->setCity('Rouen');
        $manager->persist($address);

        $manager->flush();
    }
}
