<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('Jeux de société');
        $manager->persist($category);

        $category = new Category();
        $category->setName('Jeux vidéo');
        $manager->persist($category);

        $category = new Category();
        $category->setName('Jeux de rôle');
        $manager->persist($category);

        $manager->flush();
    }
}
