<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Gift;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GiftFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $categories = $manager->getRepository(Category::class)->findAll();
        /**
         * Video games
         */
        $gift = new Gift();
        $gift->setAveragePrice(10);
        $gift->setDesignation('Minecraft');
        $gift->setDescription('Jeux de type Bac à sable et aventure, donner libre cours à votre imagination');
        $gift->setMinimumAge(7);
        $gift->setCategory($categories[1]);
        $manager->persist($gift);

        $gift = new Gift();
        $gift->setAveragePrice(80);
        $gift->setDesignation('World Of Warcraft');
        $gift->setDescription("Jeux de type mmorpg, incarnez un héros d'aezeroth");
        $gift->setMinimumAge(12);
        $gift->setCategory($categories[1]);
        $manager->persist($gift);

        /**
         * Society games
         */
        $gift = new Gift();
        $gift->setAveragePrice(15);
        $gift->setDesignation('Echec');
        $gift->setDescription('Jeux de type tour par tour et stratégie, arriverez-vous à vaincre votre adversaire ?');
        $gift->setMinimumAge(3);
        $gift->setCategory($categories[0]);
        $manager->persist($gift);

        $gift = new Gift();
        $gift->setAveragePrice(18);
        $gift->setDesignation('Monopoly');
        $gift->setDescription('Jeux de type tour par tour, devenez la personne la plus riche');
        $gift->setMinimumAge(8);
        $gift->setCategory($categories[0]);
        $manager->persist($gift);

        /**
         * Role games
         */
        $gift = new Gift();
        $gift->setAveragePrice(50);
        $gift->setDesignation('Donjons et Dragons');
        $gift->setDescription('Jeux de type jeux de role et médiéval-fantastique, survivez-vous ?');
        $gift->setMinimumAge(12);
        $gift->setCategory($categories[2]);
        $manager->persist($gift);

        $gift = new Gift();
        $gift->setAveragePrice(30);
        $gift->setDesignation('Warhammer Quest');
        $gift->setDescription("Jeux de type jeux de role et fantastique, choisissez votre personnage et partez à l'aventure");
        $gift->setMinimumAge(12);
        $gift->setCategory($categories[2]);
        $manager->persist($gift);

        $manager->flush();
    }
}
