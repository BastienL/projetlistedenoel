<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('papaNoel');
        $user1->setRoles(['ROLE_ADMIN', 'ROLE_SECRETARIAT', 'ROLE_GESTION']);
        $encrypted = $this->passwordEncoder->encodePassword($user1,'secret');
        $user1->setPassword($encrypted);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('secrétariat');
        $user2->setRoles(['ROLE_SECRETARIAT']);
        $encrypted = $this->passwordEncoder->encodePassword($user2,'secret');
        $user2->setPassword($encrypted);
        $manager->persist($user2);

        $user2 = new User();
        $user2->setUsername('gestionnaire');
        $user2->setRoles(['ROLE_GESTION']);
        $encrypted = $this->passwordEncoder->encodePassword($user2,'secret');
        $user2->setPassword($encrypted);
        $manager->persist($user2);
        $manager->flush();
    }
}
