<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Gift;
use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PersonFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $addresses = $manager->getRepository(Address::class)->findAll();
        $gifts = $manager->getRepository(Gift::class)->findAll();

        $person = new Personne();
        $person->setNameSurname('Dumas Michel');
        $person->setAddress($addresses[0]);
        $person->setDateOfBirth(new \DateTime('10-01-1950'));
        $person->addGift($gifts[0]);
        $person->addGift($gifts[2]);
        $person->setSex('male');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Dumas Sarah');
        $person->setAddress($addresses[0]);
        $person->setDateOfBirth(new \DateTime('05-02-1983'));
        $person->addGift($gifts[1]);
        $person->addGift($gifts[2]);
        $person->setSex('female');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Dumoulin Stephane');
        $person->setAddress($addresses[1]);
        $person->setDateOfBirth(new \DateTime('20-08-1990'));
        $person->addGift($gifts[1]);
        $person->addGift($gifts[3]);
        $person->setSex('male');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Deschamp Dider');
        $person->setAddress($addresses[2]);
        $person->setDateOfBirth(new \DateTime('15-12-1985'));
        $person->addGift($gifts[0]);
        $person->addGift($gifts[1]);
        $person->setSex('male');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Deschamp Laure');
        $person->setAddress($addresses[2]);
        $person->setDateOfBirth(new \DateTime('10-07-1988'));
        $person->addGift($gifts[1]);
        $person->addGift($gifts[2]);
        $person->setSex('female');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Deschamp Stephanie');
        $person->setAddress($addresses[2]);
        $person->setDateOfBirth(new \DateTime('09-05-2009'));
        $person->addGift($gifts[1]);
        $person->addGift($gifts[2]);
        $person->setSex('female');
        $manager->persist($person);

        $person = new Personne();
        $person->setNameSurname('Potter Harry');
        $person->setAddress($addresses[3]);
        $person->setDateOfBirth(new \DateTime('31-07-1989'));
        $person->addGift($gifts[0]);
        $person->addGift($gifts[3]);
        $person->setSex('male');
        $manager->persist($person);

        $manager->flush();
    }
}
