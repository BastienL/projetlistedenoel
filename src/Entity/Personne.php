<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(
 *     fields={"name_surname"},
 *     message="cette utilisateur est déjà enregistré."
 * )
 * @ORM\Entity(repositoryClass=PersonneRepository::class)
 */
class Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name_surname;

    /**
     * @ORM\Column(type="string", length=255, columnDefinition="ENUM('male', 'female', 'not indicated')")
     */
    private $sex;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_of_birth;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="personnes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fk_address;


    /**
     * @ORM\ManyToMany(targetEntity=Gift::class, mappedBy="personnes")
     */
    private $gifts;

    public function __construct()
    {
        $this->gifts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameSurname(): ?string
    {
        return $this->name_surname;
    }

    public function setNameSurname(?string $name_surname): self
    {
        $this->name_surname = $name_surname;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTime
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(?\DateTime $date_of_birth): self
    {
        $this->date_of_birth = $date_of_birth;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->fk_address;
    }

    public function setAddress(?Address $address): self
    {
        $this->fk_address = $address;

        return $this;
    }

    public function getFkAddress(): ?Address
    {
        return $this->fk_address;
    }

    public function setFkAddress(?Address $fk_address): self
    {
        $this->fk_address = $fk_address;

        return $this;
    }

    /**
     * @return Collection|Gift[]
     */
    public function getGifts(): Collection
    {
        return $this->gifts;
    }

    public function addGift(Gift $gift): self
    {
        if (!$this->gifts->contains($gift)) {
            $this->gifts[] = $gift;
            $gift->addPersonne($this);
        }

        return $this;
    }

    public function removeGift(Gift $gift): self
    {
        if ($this->gifts->removeElement($gift)) {
            $gift->removePersonne($this);
        }

        return $this;
    }

    function age(): string
    {
        $birthDate = $this->getDateOfBirth()->format("Y-m-d");
        $today = date("Y-m-d");
        $diff = date_diff(date_create($birthDate), date_create($today));
        return $diff->format('%y');
    }
}
