<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Gift::class, mappedBy="category")
     */
    private $gifts;

    public function __construct()
    {
        $this->gifts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Gift[]
     */
    public function getGifts(): Collection
    {
        return $this->gifts;
    }

    public function addGift(Gift $gift): self
    {
        if (!$this->gifts->contains($gift)) {
            $this->gifts[] = $gift;
            $gift->setGifts($this);
        }

        return $this;
    }

    public function removeGift(Gift $gift): self
    {
        if ($this->gifts->removeElement($gift)) {
            // set the owning side to null (unless already changed)
            if ($gift->getGifts() === $this) {
                $gift->setGifts(null);
            }
        }

        return $this;
    }

    public function averagePrice()
    {
        $total = 0;

        $gifts = $this->gifts;

        if (count($gifts) == null) {
            return 0;
        }

        foreach ($gifts as $gift) {
            $total += $gift->getAveragePrice();
        }

        return ($total / count($this->gifts));
    }

    public function ageIsInRange(int $min, int $max): bool
    {
        foreach ($this->gifts as $gift) {
            if ($gift->ageIsInRange($min, $max)) {
                return true;
            }
        }

        return false;
    }
}
