<?php

namespace App\Entity;

use App\Repository\GiftRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GiftRepository::class)
 */
class Gift
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minimum_age;

    /**
     * @ORM\Column(type="float")
     */
    private $average_price;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, inversedBy="personnes")
     *
     */
    private $personnes;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="gifts")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id") //reference of the column targetted here
     */
    private $category;

    public function __construct()
    {
        $this->personnes = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getMinimumAge(): ?int
    {
        return $this->minimum_age;
    }

    public function setMinimumAge(?int $minimum_age): self
    {
        $this->minimum_age = $minimum_age;

        return $this;
    }

    public function getAveragePrice(): ?float
    {
        return $this->average_price;
    }

    public function setAveragePrice(float $average_price): self
    {
        $this->average_price = $average_price;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getPersonnes(): Collection
    {
        return $this->personnes;
    }

    public function addPersonne(Personne $personne): self
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes[] = $personne;
        }

        return $this;
    }

    public function removePersonne(Personne $personne): self
    {
        $this->personnes->removeElement($personne);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function parity(): float
    {
        $peoples = $this->getPersonnes();
        $numberOfWomen = 0;

        foreach ($peoples as $people) {
            if ($people->getSex() == 'female') {
                $numberOfWomen++;
            }
        }

        if ($numberOfWomen === 0) {
            return 0;
        }

        $total = $this->getTotalPeoples();
        return ($numberOfWomen * 100 / $total);
    }

    public function getTotalPeoples(): int
    {
        $peoples = $this->getPersonnes();
        $total = count($peoples->getValues());

        return $total;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getAgeInappropriatePeoples(): ?Collection
    {
        $peoples = $this->getPersonnes();
        $ageMax = $this->getMinimumAge();

        $less = new ArrayCollection();
        foreach ($peoples as $people) {
            if ($people->age() < $ageMax) {
                $less->add($people);
            }
        }

        return $less;
    }

    public function ageIsInRange(int $min, int $max): bool
    {
        foreach ($this->personnes as $personne) {
            $age = $personne->age();
            if ($age > $min && $age < $max) {
                return true;
            }
        }

        return false;
    }
}
