<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Gift;
use App\Entity\Personne;
use App\Form\PersonneType;
use App\Repository\AddressRepository;
use App\Repository\PersonneRepository;
use Doctrine\Persistence\ManagerRegistry;
use mysql_xdevapi\CrudOperationBindable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/personne")
 */
class PersonneController extends AbstractController
{
    /**
     * @Route("/", name="personne_index", methods={"GET"})
     */
    public function index(PersonneRepository $personneRepository): Response
    {
        return $this->render('personne/index.html.twig', [
            'personnes' => $personneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="personne_new", methods={"GET","POST"})
     * @IsGranted("ROLE_SECRETARIAT")
     */
    public function new(Request $request): Response
    {
        $personne = new Personne();
        $form = $this->createForm(PersonneType::class, $personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $repo = $this->getDoctrine()->getManager()->getRepository(Address::class);

            $address = $repo->findIfExist($personne->getAddress());

            if (count($address)) {
                $personne->setAddress($address[0]);
            } else {
                $entityManager->persist($personne->getAddress());
                $entityManager->flush();
            }

            $entityManager->persist($personne);
            $entityManager->flush();

            return $this->redirectToRoute('personne_index');
        }

        return $this->render('personne/new.html.twig', [
            'personne' => $personne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="personne_show", methods={"GET"})
     */
    public function show(Personne $personne): Response
    {
        return $this->render('personne/show.html.twig', [
            'personne' => $personne,
            'address' => $personne->getAddress()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="personne_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_SECRETARIAT")
     */
    public function edit(Request $request, Personne $personne): Response
    {
        $personneName = $this->getDoctrine()->getRepository(Personne::class)->find($personne->getId())->getNameSurname();
        $form = $this->createForm(PersonneType::class, $personne, ['e' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $personne->setNameSurname($personneName);
            $entityManager->persist($personne);
            $entityManager->flush();

            return $this->redirectToRoute('personne_index');
        }

        return $this->render('personne/edit.html.twig', [
            'personne' => $personne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="personne_delete", methods={"DELETE"})
     * @IsGranted("ROLE_SECRETARIAT")
     */
    public function delete(Request $request, Personne $personne): Response
    {
        if ($this->isCsrfTokenValid('delete' . $personne->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($personne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('personne_index');
    }

    /**
     * @Route("/search", name="personne_search", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $street = $request->request->get('search_street');
        $zipCode = $request->request->get('search_zip_code');


        if (empty($street) || empty($zipCode)) {
            return $this->redirectToRoute('personne_index');
        }
        $repoAddress = $this->getDoctrine()->getManager()->getRepository(Address::class);
        $addresses = $repoAddress->findByStreetAndCity($street, $zipCode);


        foreach ($addresses as $address) {
            $personnes = $address->getPersonnes();
        }

        return $this->render('personne/index.html.twig', [
            'personnes' => $personnes,
        ]);
    }

    /**
     * @Route("/list/{id}", name="personne_wish_list", methods={"GET"})
     */
    public function wishList(Request $request, Personne $personne): Response
    {
        return $this->render('personne/list.html.twig', [
            'personne' => $personne,
            'gifts' => $personne->getGifts()
        ]);
    }

    /**
     * @Route("/list/add/{id}", name="_add", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function addGiftToTheWishList(Request $request, Personne $personne): Response
    {
        // ToDo implementation
        $giftName = $request->request->get('add');

        $giftToAdd = $this->getDoctrine()->getRepository(Gift::class)->findByDesignation($giftName);

        if (count($giftToAdd) === 0) {
            return $this->render('personne/list.html.twig', [
                'personne' => $personne,
                'gifts' => $personne->getGifts(),
                'message' => 'This gift is not found'
            ]);
        }

        if ($personne->getGifts()->contains($giftToAdd[0])) {
            return $this->render('personne/list.html.twig', [
                'personne' => $personne,
                'gifts' => $personne->getGifts(),
                'message' => 'This gift is already added'
            ]);
        }

        $personne->addGift($giftToAdd[0]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($personne);
        $entityManager->flush();


        return $this->render('personne/list.html.twig', [
            'personne' => $personne,
            'gifts' => $personne->getGifts()
        ]);
    }

    /**
     * @Route("/list/remove/{id_personne}/{id_gift}", name="_delete_gift", methods={"POST"})
     * @ParamConverter("personne", options={"mapping": {"id_personne"   : "id"}})
     * @ParamConverter("gift", options={"mapping": {"id_gift"   : "id"}})
     * @param Request $request
     * @return Response
     */
    public function removeGiftToTheWishList(Request $request, Personne $personne, Gift $gift): Response
    {
        $personne->removeGift($gift);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($personne);
        $entityManager->flush();


        return $this->render('personne/list.html.twig', [
            'personne' => $personne,
            'gifts' => $personne->getGifts()
        ]);
    }
}
