<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 * @IsGranted("ROLE_GESTION")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/", name="category_index", methods={"GET"})
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="category_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_show", methods={"GET"})
     */
    public function show(Category $category): Response
    {
        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Category $category): Response
    {
        if ($this->isCsrfTokenValid('delete' . $category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_index');
    }

    public function getNumberOfGiftInCategory()
    {

    }

    /**
     * @Route("/prise/increase/{id_category}", name="category_prise", methods={"POST"})
     * @ParamConverter("category", options={"mapping": {"id_category"   : "id"}})
     * @param Request $request
     * @return Response
     */
    public function IncreasePrice(Request $request, Category $category)
    {
        $value = $request->request->get('value');
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($category->getGifts() as $gift) {
            $curentPrise = $gift->getAveragePrice();
            $total = $curentPrise + ($curentPrise * $value / 100);

            $gift->setAveragePrice($total);
            $entityManager->persist($gift);
            $entityManager->flush();
        }
        return $this->render('category/show.html.twig', [
            'category' => $category,
            'message' => 'Done',
            'value' => $value
        ]);
    }


    /**
     * @Route("/search/by_age_range", name="_search_by_age_range", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function searchByAgeRange(Request $request)
    {
        $minAge = $request->request->get('min');
        $maxAge = $request->request->get('max');

        if ($minAge > $maxAge) {
            $value = $minAge;
            $minAge = $maxAge;
            $maxAge = $value;
        }

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();


        $categoryFind = new ArrayCollection();
        foreach ($categories as $category) {
            if ($category->ageIsInRange($minAge, $maxAge)) {
                $categoryFind->add($category);
            }
        }

        return $this->render('category/index.html.twig', [
            'categories' => $categoryFind,
        ]);
    }
}
