<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Gift;
use App\Entity\Personne;
use App\Form\GiftType;
use App\Repository\GiftRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gift")
 * @IsGranted("ROLE_GESTION")
 */
class GiftController extends AbstractController
{
    /**
     * @Route("/", name="gift_index", methods={"GET"})
     */
    public function index(GiftRepository $giftRepository): Response
    {
        return $this->render('gift/index.html.twig', [
            'gifts' => $giftRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="gift_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $gift = new Gift();
        $form = $this->createForm(GiftType::class, $gift);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $repo = $this->getDoctrine()->getManager()->getRepository(Category::class);

            $category = $repo->findIfExist($gift->getCategory());

            if (count($category)) {
                $gift->setCategory($category[0]);
            } else {
                $entityManager->persist($gift->getCategory());
                $entityManager->flush();
            }
            $entityManager->persist($gift);
            $entityManager->flush();

            return $this->redirectToRoute('gift_index');
        }

        return $this->render('gift/new.html.twig', [
            'gift' => $gift,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="gift_show", methods={"GET"})
     */
    public function show(Gift $gift): Response
    {
        return $this->render('gift/show.html.twig', [
            'gift' => $gift,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="gift_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Gift $gift): Response
    {
        $designtion = $this->getDoctrine()->getRepository(Gift::class)->find($gift->getId())->getDesignation();
        $form = $this->createForm(GiftType::class, $gift, ['e' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $gift->setDesignation($designtion);
            $entityManager->persist($gift);
            $entityManager->flush();

            return $this->redirectToRoute('gift_index');
        }

        return $this->render('gift/edit.html.twig', [
            'gift' => $gift,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="gift_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Gift $gift): Response
    {
        if ($this->isCsrfTokenValid('delete' . $gift->getId(), $request->request->get('_token')) && count($gift->getPersonnes()) === 0) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gift);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gift_index');
    }

    /**
     * @Route("/list_age_inappropriate/{id}", name="gift_list_age_inap", methods={"GET"})
     */
    public function listOfAgeInappropriatePeoples(Request $request, Gift $gift)
    {
        $less = $gift->getAgeInappropriatePeoples();

        if (!$less) {
            return $this->render('personne/index.html.twig', [
                'personnes' => null,
                'message' => 'Toutes les personne on le bon age !!'
            ]);
        }

        return $this->render('personne/index.html.twig', [
            'personnes' => $less,
            'message' => 'La ou les personnes suivantes sont trop jeune'
        ]);
    }
}
