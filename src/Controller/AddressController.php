<?php

namespace App\Controller;

use App\Entity\Address;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/address")
 * @IsGranted("ROLE_SECRETARIAT")
 */
class AddressController extends AbstractController
{
    /**
     * @Route("/", name="address_index", methods={"GET"})
     * @param AddressRepository $addressRepository
     * @return Response
     */
    public function index(AddressRepository $addressRepository): Response
    {
        return $this->render('address/index.html.twig', [
            'addresses' => $addressRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="address_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($address);
            $entityManager->flush();

            return $this->redirectToRoute('address_index');
        }

        return $this->render('address/new.html.twig', [
            'address' => $address,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="address_show", methods={"GET"})
     * @param Address $address
     * @return Response
     */
    public function show(Address $address): Response
    {
        return $this->render('address/show.html.twig', [
            'address' => $address,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="address_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Address $address
     * @return Response
     */
    public function edit(Request $request, Address $address): Response
    {
        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('address_index');
        }

        return $this->render('address/edit.html.twig', [
            'address' => $address,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="address_delete", methods={"DELETE"})
     * @param Request $request
     * @param Address $address
     * @return Response
     */
    public function delete(Request $request, Address $address): Response
    {

        if ($this->isCsrfTokenValid('delete' . $address->getId(), $request->request->get('_token')) && count($address->getPersonnes()) === 0) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($address);
            $entityManager->flush();

            return $this->redirectToRoute('address_index');
        } else {
            return $this->redirectToRoute('address/index.html.twig');
        }

    }


    /**
     * @Route("/search", name="_search", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $data = $request->request->get('search');

        if (empty($data)) {
            return $this->redirectToRoute('address_index');
        }

        $repo = $this->getDoctrine()->getManager()->getRepository(Address::class);

        return $this->render('address/index.html.twig', [
            'addresses' => $repo->findByCity($data),
        ]);
    }
}
